# Mediascrape

Scrapes the ARD Mediathek website to get the direct video URLs for the movie highlights and serves the results via Jetty as html.

To generate a Java executable install:

- Java
- [Leiningen](https://github.com/technomancy/leiningen)

Get the code:

    git clone https://github.com/steckerhalter/mediascrape.git
    cd mediascrape

And build the JAR:

    lein uberjar

Run the generated file with Java:

    java -jar mediascrape-0.0.1-standalone.jar

This should start a webserver on: http://localhost:8080
