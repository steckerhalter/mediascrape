(ns mediascrape
  (:require [net.cgrand.enlive-html :as html])
  (:use ring.adapter.jetty)
  (:use ring.middleware.params)
  (:gen-class))

(def base-url "http://www.ardmediathek.de")
(def highlights-url (str base-url "/ard/servlet/ajax-cache/4585472/view=switch/index.html"))

(defn fetch-url [url]
  (try
    (html/html-resource (java.net.URL. url))
    (catch Exception e)))

(defn highlights []
  (for [[title source rating]
        (partition-all 3 (html/select (fetch-url highlights-url) #{[:.mt-title :a] [:.mt-source] [:.mt-rating]}))]
    {:title (first (:content title))
     :url (str base-url (:href (:attrs title)))
     :source (first (:content source))
     :rating (first (:content rating))}
    ))

(defn get-video [url]
  (let [js (first
            (:content
             (first
              (html/select
               (fetch-url url)
               [:.mt-player_container :script]))))]
    (when js
      (last
       (or (re-find #"addMediaStream\(1, 2, \"\", \"(.+?)\"" js)
           (re-find #"addMediaStream\(1, 1, \"\", \"(.+?)\"" js)
           (re-find #"addMediaStream\(0, 2, \"\", \"(.+?)\"" js)
           (re-find #"addMediaStream\(0, 1, \"\", \"(.+?)\"" js))))))

(defn add-video [highlight]
  (let [video (get-video (:url highlight))]
    (when video (assoc highlight :video video))))

(defn get-entries []
  (filter identity (map add-video (highlights))))

(def entries (get-entries))

(def preamble "
<html>
<head><title>ARD Mediathek Film Highlights</title></head>
<body>
<a href=\"/\"><h1>ARD Mediathek Film Highlights</h1></a>
<a href=\"/?refresh=true\">Refresh the source data</a>")

(def postamble "
</body>
</html>")

(defn render-item [item]
  (format "
<h3><a href=\"%s\">%s</a> %s / %s</h3>
"
          (:video item)
          (:title item)
          (:source item)
          (:rating item)))

(defn render-page [params]
  (when (= "true" (params "refresh"))
    (def entries (get-entries)))
  (str
   preamble
   (apply str (map render-item entries))
   postamble))

(defn handler [{params :params}]
  {:status  200
   :headers {"Content-Type" "text/html"}
   :body    (render-page params)})

(def app
  (wrap-params handler))

(defn -main []
  (run-jetty app {:port 8080 :join? false}))
