(defproject mediascrape "0.0.1"
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [enlive "1.1.1"]
                 [ring/ring-core "1.2.1"]
                 [ring/ring-jetty-adapter "1.2.1"]]
  :repl-options {:init-ns mediascrape}
  :main mediascrape
  :aot [mediascrape]
  :manifest {"Built-By" "steckerhalter"})
